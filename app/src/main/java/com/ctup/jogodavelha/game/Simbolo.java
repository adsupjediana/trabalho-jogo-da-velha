package com.ctup.jogodavelha.game;

import android.widget.ImageButton;

import com.ctup.jogodavelha.R;

/**
 * Created by jedi on 29/03/2017.
 *
 * Especifica os símbolos do jogo a serem exibidos
 */

public enum Simbolo {


    X,
    O;


    public int mostrarImagem() {
        if(this == Simbolo.X)
            return R.drawable.letraxarial;

        return R.drawable.letraoarial;
    }
}
