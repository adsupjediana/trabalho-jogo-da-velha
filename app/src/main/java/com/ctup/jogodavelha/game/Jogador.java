package com.ctup.jogodavelha.game;

/**
 * Created by jedi on 29/03/2017.
 *
 * Um jogador. Deve-se informar o seu apelido e o símbolo que ele usará para jogar
 */

public class Jogador {

    private String apelido;
    private Simbolo simbolo;

    /**
     * Cria uma nova instância de um @see {@link Jogador}
     * @param apelido O Apelido que o jogador usará no jogo
     * @param simbolo O símbolo que ele usará no jogo
     */
    public Jogador(String apelido, Simbolo simbolo) {
        this.apelido = apelido;
        this.simbolo = simbolo;
    }

    /**
     *
     * @return O apelido do jogador
     */
    public String getApelido() {
        return apelido;
    }

    /**
     *
     * @return O símbolo do jogador
     */
    public Simbolo getSimbolo() {
        return simbolo;
    }

    @Override
    public boolean equals(Object obj) {

        try {
            Jogador outro = (Jogador)obj;
            return outro.getApelido().equalsIgnoreCase(this.apelido)
                    && outro.getSimbolo().equals(this.simbolo);
        }catch (Exception ex){
            return false;
        }
    }
}
