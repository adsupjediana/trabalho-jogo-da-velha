package com.ctup.jogodavelha.game;

/**
 * Created by jedi on 29/03/2017.
 * <p>
 * Gerencia o Jogo da Velha
 */

public class JogoDaVelha {

    private Jogador jogador1;
    private Jogador jogador2;
    private Jogador vencedor;
    private Jogador ultimoJogador;
    private int jogadasRealizadas;
    private boolean jogoEncerrado;

    private Simbolo[][] tabuleiro = new Simbolo[3][3];

    /**
     * Cria uma nova instância de um @see {@link JogoDaVelha}
     *
     * @param jogador1 O primeiro jogador
     * @param jogador2 O segundo jogador
     */
    public JogoDaVelha(Jogador jogador1, Jogador jogador2) {
        this.validaJogadores(jogador1, jogador2);
        this.jogador1 = jogador1;
        this.jogador2 = jogador2;
    }

    /**
     * Executa uma jogada do jogo
     *
     * @param jogador O jogador que executou a jogada
     * @return Um @see {@link ResultadoJogada}
     */
    public ResultadoJogada executarJogada(Jogador jogador, int posLinha, int posColuna) {

        // verifica se o jogador é válido
        if(!jogadorEstaRegistrado(jogador)){
            throw new IllegalArgumentException("O jogador não está cadastrado no jogo");
        }

        // verifica se é a vez deste jogador jogar
        if(!jogadorQueDeveJogar(jogador)){
            throw new IllegalArgumentException("Não é a vez deste jogador");
        }

        // verifica se a posição está no tabuleiro
        if (!posicaoValida(posLinha, posColuna)) {
            return ResultadoJogada.POSICAO_INVALIDA;
        }

        // verifica se a posição já não foi marcada
        if (posicaoOucupada(posLinha, posColuna)) {
            return ResultadoJogada.POSICAO_INVALIDA;
        }

        // marca a jogada como o ultimo que jogou
        this.ultimoJogador = jogador;

        // valida a execucao da jogada
        jogadasRealizadas++;
        efetuaMarcacao(jogador.getSimbolo(), posLinha, posColuna);

        // a única possibilidade de fechar uma posicao é a partir da 5 jogada
        if (jogadasRealizadas < 5) {
            return ResultadoJogada.AGUARDANDO_PROXIMA;
        }

        try {

            // se o jogo não foi encerrado, aguarda a próxima
            if (!jogoEncerrado(posLinha, posColuna)) {
                return ResultadoJogada.AGUARDANDO_PROXIMA;
            }

            // do contrário, determina o vencedor
            this.vencedor = jogador;
            // e encerra o jogo
            this.jogoEncerrado = true;
            return ResultadoJogada.ENCERRADO;

        }catch (JogoEmpatado ex){
            // ou finaliza o jogo como empatado
            this.jogoEncerrado = true;
            return ResultadoJogada.ENCERRADO_EMPATADO;
        }
    }

    /**
     * Retorna o jogador que venceu a partida
     * @return O jogador que venceu a partida
     */
    public Jogador getVencedor() {
        return vencedor;
    }

    /**
     * Verifica se o jogo está encerrado
     * @return true caso o jogo esteja encerrado
     */
    public boolean jogoEncerrado(){
        return this.jogoEncerrado;
    }

    /**
     * Reinicia a partida.
     * O tabuleiro é limpo e os dados do jogo anterior são resetados
     */
    public void resetarPartida(){
        limparTabuleiro();
        this.jogoEncerrado = false;
        this.vencedor = null;
        this.ultimoJogador = null;
        this.jogadasRealizadas = 0;
    }

    private void limparTabuleiro() {
        for (int i = 0; i< 3; i++){
            for(int j = 0; j < 3; j++){
                this.tabuleiro[i][j] = null;
            }
        }
    }

    /**
     * Valida se é o jogador que está na vez de jogar
     * @param jogador O jogador
     * @return true se o jogador for o que deve jogar
     */
    private boolean jogadorQueDeveJogar(Jogador jogador) {
        return !jogador.equals(this.ultimoJogador);
    }

    /**
     * Verifica se o jogador está registrado no jogo
     * @param jogador O jogador a ser testado
     * @return TRUE caso o jogador esteja registrado
     */
    private boolean jogadorEstaRegistrado(Jogador jogador) {
        return this.jogador1.equals(jogador) || this.jogador2.equals(jogador);
    }

    private boolean jogoEncerrado(int posLinha, int posColuna) throws JogoEmpatado {

        Simbolo simbolo = tabuleiro[posLinha][posColuna];
        if(simbolo == null){
            throw new NullPointerException("Posição inválida ou ainda não assinalada");
        }

        if(fechouLinha(simbolo, posLinha)){
            return true;
        }

        if(fechouColuna(simbolo, posColuna)){
            return true;
        }

        if(fechouDiagonal(simbolo, posLinha, posColuna)){
            return true;
        }

        if(tabuleiroCheio()){
            throw new JogoEmpatado();
        }

        return false;
    }

    private boolean tabuleiroCheio() {
        return this.jogadasRealizadas == 9;
    }

    private boolean fechouDiagonal(Simbolo simbolo, int posLinha, int posColuna) {

        if(marcouNoCentro(posLinha, posColuna)){
            return fechouDiagonalPrincipal(simbolo) || fechouDiagonalSecundaria(simbolo);
        }

        if(marcouNaDiagonalPrincipal(posLinha, posColuna)){
            return fechouDiagonalPrincipal(simbolo);
        }

        if(marcouNaDiagonalSecundaria(posLinha, posColuna)){
            return fechouDiagonalSecundaria(simbolo);
        }

        return false;
    }

    private boolean fechouDiagonalSecundaria(Simbolo simbolo) {

        int i = 0;
        int j = 2;

        while(j >= 0){
            if(tabuleiro[i][j] != simbolo){
                return false;
            }
            i++;
            j--;
        }

        return true;
    }

    private boolean marcouNaDiagonalSecundaria(int posLinha, int posColuna) {
        return marcouNoCentro(posLinha, posColuna)
                || (posLinha == 0 && posColuna == 2)
                || (posLinha == 2 && posColuna == 0);
    }

    private boolean marcouNaDiagonalPrincipal(int posLinha, int posColuna) {
        return posLinha == posColuna;
    }

    private boolean fechouDiagonalPrincipal(Simbolo simbolo) {

        for (int i = 0; i < 3; i++){
            if(tabuleiro[i][i] != simbolo){
                return false;
            }
        }

        return true;
    }

    private boolean marcouNoCentro(int posLinha, int posColuna) {
        return posColuna == 1 && posLinha == 1;
    }

    private boolean fechouColuna(Simbolo simbolo, int posColuna) {

        for (int i = 0; i < 3; i++){
            if(tabuleiro[i][posColuna] != simbolo){
                return false;
            }
        }

        return true;
    }

    private boolean fechouLinha(Simbolo simbolo, int posLinha) {

        for (int i = 0; i < 3; i++){
            if(tabuleiro[posLinha][i] != simbolo){
                return false;
            }
        }

        return true;
    }

    /**
     * Efetua a marcação de um {@link Simbolo} no tabuleiro
     *
     * @param simbolo   O Simbolo a ser marcado
     * @param posLinha  A posição da linha
     * @param posColuna A posição da Coluna
     */
    private void efetuaMarcacao(Simbolo simbolo, int posLinha, int posColuna) {
        this.tabuleiro[posLinha][posColuna] = simbolo;
    }

    /**
     * Verifica se a posição está oucupada
     *
     * @param posLinha  A posição da linha que se deseja testar
     * @param posColuna A posição da coluna que se deseja testar
     * @return True se a linha está oucupada
     */
    private boolean posicaoOucupada(int posLinha, int posColuna) {
        return this.tabuleiro[posLinha][posColuna] != null;
    }

    /**
     * Verifica se a jogada foi informada num range válido
     *
     * @param posLinha  A posição para a linha
     * @param posColuna A posição para a coluna
     * @return true se a posição está dentro do tabuleiro
     */
    private boolean posicaoValida(int posLinha, int posColuna) {
        return posLinha < 3 && posColuna < 3;
    }

    /**
     * Verifica se os jogadores informados são válidos
     *
     * @param jogador1 O primeiro jogador
     * @param jogador2 O segundo jogador
     */
    private void validaJogadores(Jogador jogador1, Jogador jogador2) {

        if (jogador1 == null || jogador2 == null) {
            throw new IllegalArgumentException("Um ou mais jogadores são null");
        }

        if (jogador1.equals(jogador2)) {
            throw new IllegalArgumentException("Os jogadores são iguais");
        }
    }

    private class JogoEmpatado extends Exception {

    }
}
