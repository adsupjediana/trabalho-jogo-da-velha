package com.ctup.jogodavelha.game;

/**
 * Created by jedi on 29/03/2017.
 *
 * Especifica o resultado do jogo
 */

public enum ResultadoJogada {

    POSICAO_INVALIDA,
    AGUARDANDO_PROXIMA,
    ENCERRADO_EMPATADO,
    ENCERRADO
}
