package com.ctup.jogodavelha.activities.novo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.ctup.jogodavelha.R;
import com.ctup.jogodavelha.activities.jogo.JogoActivity;


public class NovoActivity extends AppCompatActivity {

    ImageButton imageButton;
    EditText editText1;
    EditText editText2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);

      findViewById(R.id.btnJogar).setOnClickListener(new handleClick());
       editText1 = (EditText) findViewById(R.id.editJogador1);
        editText2 = (EditText) findViewById(R.id.editJogador2);


    }

    private class handleClick implements View.OnClickListener {
        public void onClick(View view) {
            String jogador1 = editText1.getText().toString();
            String jogador2 = editText2.getText().toString();
            if (editText1.length() > 0 && editText2.length() > 0) {
                Intent intent = new Intent(NovoActivity.this, JogoActivity.class);
                        intent.putExtra("nome1", jogador1);
                intent.putExtra("nome2", jogador2);
                startActivity(intent);



            } else {

                Toast.makeText(NovoActivity.this, "Preencha os dois nomes!", Toast.LENGTH_LONG).show();
            }
        }

    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("SecundaryActivity", "onCreate");
    }

    public void onRestart() {
        super.onRestart();
        Log.d("SecundaryActivity", "onRestart");
    }

    public void onResume() {
        super.onResume();
        Log.d("SecundaryActivity", "onResume");
    }

    public void onPause() {
        super.onPause();
        Log.d("SecundaryActivity", "onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d("SecundaryActivity", "onStop");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("SecundaryActivity", "onDestroy");

    }
}
