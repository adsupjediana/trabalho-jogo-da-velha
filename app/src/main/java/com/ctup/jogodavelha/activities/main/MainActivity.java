package com.ctup.jogodavelha.activities.main;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

import com.ctup.jogodavelha.R;
import com.ctup.jogodavelha.activities.novo.NovoActivity;

public class MainActivity extends AppCompatActivity implements Runnable {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Handler handler = new Handler();
        handler.postDelayed(this, 3000);
    }

    public void run() {
        startActivity(new Intent(this, NovoActivity.class));
        finish();
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("MainActivity", "onCreate");
    }

    public void onRestart() {
        super.onRestart();
        Log.d("MainActivity", "onRestart");
    }

    public void onResume() {
        super.onResume();
        Log.d("MainActivity", "onResume");
    }

    public void onPause() {

        super.onPause();
        Log.d("MainActivity", "onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d("MainActivity", "onStop");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("MainActivity", "onDestroy");
    }
}
