package com.ctup.jogodavelha.activities.jogo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.ctup.jogodavelha.R;
import com.ctup.jogodavelha.game.Jogador;
import com.ctup.jogodavelha.game.JogoDaVelha;
import com.ctup.jogodavelha.game.ResultadoJogada;
import com.ctup.jogodavelha.game.Simbolo;

import java.util.List;

import butterknife.BindView;
import butterknife.BindViews;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class JogoActivity extends AppCompatActivity {

    private final String TAG = "JogoActivity";

    @BindViews({R.id.btn00, R.id.btn01, R.id.btn02, R.id.btn10, R.id.btn11, R.id.btn12, R.id.btn20, R.id.btn21, R.id.btn22})
    List<ImageButton> tabuleiro;

    @BindView(R.id.btnNovaPartida)
    Button botaNovaPartida;
    TextView tv_1;
    TextView tv_2;
    TextView tv_3;
    Jogador jogadorX;
    Jogador jogadorO;
    Jogador proximoJogador;
    Jogador vencedor;
    JogoDaVelha jogoDaVelha;
    int pontuacaoX, pontuacaoO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_jogo);
        ButterKnife.bind(this);
        tv_1 = (TextView) findViewById(R.id.tv_jogador1);
        tv_2 = (TextView) findViewById(R.id.tv_jogador2);
        tv_3 = (TextView) findViewById(R.id.tv_proximoJogador);


        Intent intent = getIntent();

        Bundle extra = getIntent().getExtras();
        String nome1 = extra.getString("nome1");
        String nome2 = extra.getString("nome2");
        tv_1.setText("Jogador X: " + "\n" + nome1);
        tv_2.setText("Jogador O: " + "\n" + nome2);

        jogadorX = new Jogador(nome1, Simbolo.X);
        jogadorO = new Jogador(nome2, Simbolo.O);
        jogoDaVelha = new JogoDaVelha(jogadorX, jogadorO);

    }

    @OnClick({R.id.btn00, R.id.btn01, R.id.btn02, R.id.btn10, R.id.btn11, R.id.btn12, R.id.btn20, R.id.btn21, R.id.btn22})
    protected void onClick(View view) {

        if (jogoDaVelha.jogoEncerrado()) {
            return;
        }

        String tag = view.getTag().toString();
        int linha = Integer.parseInt(tag.substring(6, 7).toString());
        int coluna = Integer.parseInt(tag.substring(7, 8).toString());

        ResultadoJogada jogada = jogoDaVelha.executarJogada(getProximoJogador(), linha, coluna);


        processarResultadoJogada(jogada, getProximoJogador(), linha, coluna);


    }

    @OnClick(R.id.btnNovaPartida)
    public void onClickNovaPartida(View view) {
        jogoDaVelha.resetarPartida();
        limparTabuleiro();
        proximoJogador = null;
        tv_3.setText("");

    }

    private void limparTabuleiro() {
        for (ImageButton limpar : tabuleiro) {
            limpar.setImageResource(0);

        }
    }

    private Jogador getProximoJogador() {
        if (proximoJogador == null) {
            proximoJogador = jogadorX;


        }
        return proximoJogador;
    }

    private void processarResultadoJogada(ResultadoJogada jogada, Jogador jogador, int linha, int coluna) {
        switch (jogada) {

            case POSICAO_INVALIDA:
                Log.d(TAG, ResultadoJogada.POSICAO_INVALIDA.toString());
                notificarParaJogarNovamente();
                break;
            case AGUARDANDO_PROXIMA:
                Log.d(TAG, ResultadoJogada.AGUARDANDO_PROXIMA.toString());
                atualizarTabuleiro(jogador.getSimbolo(), linha, coluna);
                atualizarProximoJogador(jogador);
                break;
            case ENCERRADO_EMPATADO:
                Log.d(TAG, ResultadoJogada.ENCERRADO_EMPATADO.toString());
                atualizarTabuleiro(jogador.getSimbolo(), linha, coluna);
                encerrarJogoEmpatado();
                break;
            case ENCERRADO:
                Log.d(TAG, ResultadoJogada.ENCERRADO.toString());
                atualizarTabuleiro(jogador.getSimbolo(), linha, coluna);
                encerrarJogo();
                break;
        }

    }

    private void encerrarJogo() {
        incrementarPontuação();

        Toast.makeText(this, "Fim do jogo", Toast.LENGTH_SHORT).show();

        tv_3.setText("Venceu o jogador: " + jogoDaVelha.getVencedor().getSimbolo());
    }

    private void incrementarPontuação() {
        Jogador vencedor = jogoDaVelha.getVencedor();
        if (vencedor.equals(jogadorX)) {
            //atualizar  pontuação na view
            pontuacaoX++;
            return;
        }
        pontuacaoO++;
    }

    private void notificarParaJogarNovamente() {
        Toast.makeText(this, "Jogue novamente", Toast.LENGTH_SHORT).show();
    }

    private void atualizarTabuleiro(Simbolo simbolo, int linha, int coluna) {

        LinearLayout layout = (LinearLayout) findViewById(R.id.activity_jogo);
        String tag = "celula" + linha + "" + coluna;
        ImageButton botao = (ImageButton) layout.findViewWithTag(tag);
        botao.setImageResource(simbolo.mostrarImagem());

    }

    private void atualizarProximoJogador(Jogador jogador) {
        if (jogador.getSimbolo().equals(Simbolo.X)) {
            proximoJogador = jogadorO;
            tv_3.setText("Vez do jogador \"O\" ");
            return;
        }
        proximoJogador = jogadorX;
        tv_3.setText("Vez do jogador \"X\" ");
    }

    private void encerrarJogoEmpatado() {

        Toast.makeText(this, "Deu velha!", Toast.LENGTH_LONG).show();
        tv_3.setText("");
    }

    @Override
    public void onStart() {
        super.onStart();
        Log.d("SecundaryActivity", "onCreate");
    }

    public void onRestart() {
        super.onRestart();
        Log.d("SecundaryActivity", "onRestart");
    }

    public void onResume() {
        super.onResume();
        Log.d("SecundaryActivity", "onResume");
    }


    public void onPause() {
        super.onPause();
        Log.d("SecundaryActivity", "onPause");
    }

    public void onStop() {
        super.onStop();
        Log.d("SecundaryActivity", "onStop");
    }

    public void onDestroy() {
        super.onDestroy();
        Log.d("SecundaryActivity", "onDestroy");

    }


    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }


}
