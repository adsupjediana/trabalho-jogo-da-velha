package com.ctup.jogodavelha.stubs;

import com.ctup.jogodavelha.game.Jogador;
import com.ctup.jogodavelha.game.Simbolo;

/**
 * Created by jedi on 04/04/2017.
 */

public class JogadorValues {
    public static final Jogador JogadorXis = new Jogador("jogadorXis", Simbolo.X);
    public static final Jogador JogadorBolinha = new Jogador("jogadorBolinha", Simbolo.O);
}
