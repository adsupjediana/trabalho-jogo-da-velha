package com.ctup.jogodavelha;

import com.ctup.jogodavelha.game.Jogador;
import com.ctup.jogodavelha.game.JogoDaVelha;
import com.ctup.jogodavelha.game.ResultadoJogada;
import com.ctup.jogodavelha.stubs.JogadorValues;

import org.junit.Assert;
import org.junit.Test;

/**
 * Created by jedi on 04/04/2017.
 * <p>
 * Testes para o Jogo da velha
 */

public class JogoTest {

    @Test(expected = IllegalArgumentException.class)
    public void ambos_jogadores_nao_podem_ser_nulos() {
        new JogoDaVelha(null, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void primeiro_jogador_nao_pode_ser_nulo() {
        new JogoDaVelha(null, JogadorValues.JogadorBolinha);
    }

    @Test(expected = IllegalArgumentException.class)
    public void segundo_jogador_nao_pode_ser_nulo() {
        new JogoDaVelha(JogadorValues.JogadorBolinha, null);
    }

    @Test(expected = IllegalArgumentException.class)
    public void jogadores_nao_podem_ser_repetidos() {
        new JogoDaVelha(JogadorValues.JogadorXis, JogadorValues.JogadorXis);
    }

    @Test(expected = IllegalArgumentException.class)
    public void jogador_nao_pode_ser_nulo() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);
        jogo.executarJogada(null, 1, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void jogador_nao_deve_jogar_duas_vezes() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);
        jogo.executarJogada(jogador1, 1, 1);
        jogo.executarJogada(jogador1, 1, 2);
    }

    @Test
    public void posicao_deve_estar_no_tabuleiro() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador1, 4, 4);
        Assert.assertEquals(ResultadoJogada.POSICAO_INVALIDA, resultadoJogada);
    }

    @Test
    public void posicao_oucupada_nao_deve_ser_marcada() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);
        jogo.executarJogada(jogador1, 1, 1);

        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador2, 1, 1);
        Assert.assertEquals(ResultadoJogada.POSICAO_INVALIDA, resultadoJogada);
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_linha1_x() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);

        // x 0 0
        jogo.executarJogada(jogador1, 0, 0);

        // O 1 0
        jogo.executarJogada(jogador2, 1, 0);

        // x 0 1
        jogo.executarJogada(jogador1, 0, 1);

        // O 2 1
        jogo.executarJogada(jogador2, 2, 1);

        // x 0 2 - Aqui a primeira linha deve estar fechada
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador1, 0, 2);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogador1, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_linha2_x() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);

        // x 1 0
        jogo.executarJogada(jogador1, 1, 0);

        // O 0 0
        jogo.executarJogada(jogador2, 0, 0);

        // x 1 1
        jogo.executarJogada(jogador1, 1, 1);

        // O 2 2
        jogo.executarJogada(jogador2, 2, 2);

        // x 1 2 - Aqui a segunda linha deve estar fechada
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador1, 1, 2);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogador1, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_linha3_x() {
        Jogador jogador1 = JogadorValues.JogadorXis;
        Jogador jogador2 = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);

        // x 2 0
        jogo.executarJogada(jogador1, 2, 0);

        // O 0 0
        jogo.executarJogada(jogador2, 0, 0);

        // x 2 1
        jogo.executarJogada(jogador1, 2, 1);

        // O 1 2
        jogo.executarJogada(jogador2, 1, 2);

        // x 2 2 - Aqui a terceira linha deve estar fechada
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador1, 2, 2);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogador1, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_coluna1_O() {
        Jogador jogador1 = JogadorValues.JogadorBolinha;
        Jogador jogador2 = JogadorValues.JogadorXis;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);

        // O 0 0
        jogo.executarJogada(jogador2, 0, 0);

        // X 0 1
        jogo.executarJogada(jogador1, 0, 1);

        // O 1 0
        jogo.executarJogada(jogador2, 1, 0);

        // X 0 2
        jogo.executarJogada(jogador1, 0, 2);

        // O 2 0 - Aqui a primeira coluna deve estar fechada
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador2, 2, 0);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogador2, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_coluna2_O() {
        Jogador jogador1 = JogadorValues.JogadorBolinha;
        Jogador jogador2 = JogadorValues.JogadorXis;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);

        // O 0 1
        jogo.executarJogada(jogador2, 0, 1);

        // X 2 0
        jogo.executarJogada(jogador1, 2, 0);

        // O 1 1
        jogo.executarJogada(jogador2, 1, 1);

        // X 0 2
        jogo.executarJogada(jogador1, 0, 2);

        // O 2 1 - Aqui a primeira coluna deve estar fechada
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador2, 2, 1);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogador2, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_coluna3_O() {
        Jogador jogador1 = JogadorValues.JogadorBolinha;
        Jogador jogador2 = JogadorValues.JogadorXis;

        JogoDaVelha jogo = new JogoDaVelha(jogador1, jogador2);

        // O 0 2
        jogo.executarJogada(jogador2, 0, 2);

        // X 2 0
        jogo.executarJogada(jogador1, 2, 0);

        // O 1 2
        jogo.executarJogada(jogador2, 1, 2);

        // X 0 1
        jogo.executarJogada(jogador1, 0, 1);

        // O 2 2 - Aqui a primeira coluna deve estar fechada
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogador2, 2, 2);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogador2, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_diagonal_principal_X() {
        Jogador jogadorX = JogadorValues.JogadorXis;
        Jogador jogadorO = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogadorX, jogadorO);


        jogo.executarJogada(jogadorX, 0, 0);
        jogo.executarJogada(jogadorO, 2, 0);
        jogo.executarJogada(jogadorX, 1, 1);
        jogo.executarJogada(jogadorO, 0, 2);
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogadorX, 2, 2);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogadorX, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_possuir_vencedor_se_fechar_diagonal_secundaria_O() {
        Jogador jogadorO = JogadorValues.JogadorBolinha;
        Jogador jogadorX = JogadorValues.JogadorXis;

        JogoDaVelha jogo = new JogoDaVelha(jogadorO, jogadorX);


        jogo.executarJogada(jogadorO, 0, 2);
        jogo.executarJogada(jogadorX, 2, 2);
        jogo.executarJogada(jogadorO, 1, 1);
        jogo.executarJogada(jogadorX, 0, 0);
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogadorO, 2, 0);
        Assert.assertEquals(ResultadoJogada.ENCERRADO, resultadoJogada);
        Assert.assertEquals(jogadorO, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_seguir_se_nao_tiver_vencedor_apos_jogada() {
        Jogador jogadorX = JogadorValues.JogadorXis;
        Jogador jogadorO = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogadorX, jogadorO);

        jogo.executarJogada(jogadorX, 0, 0);
        jogo.executarJogada(jogadorO, 2, 2);
        jogo.executarJogada(jogadorX, 0, 2);
        jogo.executarJogada(jogadorO, 0, 1);
        jogo.executarJogada(jogadorX, 1, 1);
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogadorO, 2, 0);

        Assert.assertEquals(ResultadoJogada.AGUARDANDO_PROXIMA, resultadoJogada);
        Assert.assertEquals(null, jogo.getVencedor());
        Assert.assertFalse(jogo.jogoEncerrado());
    }

    @Test
    public void jogo_deve_retornar_empate_se_empatado() {
        Jogador jogadorX = JogadorValues.JogadorXis;
        Jogador jogadorO = JogadorValues.JogadorBolinha;

        JogoDaVelha jogo = new JogoDaVelha(jogadorX, jogadorO);

        jogo.executarJogada(jogadorX, 0, 0);
        jogo.executarJogada(jogadorO, 2, 2);
        jogo.executarJogada(jogadorX, 0, 2);
        jogo.executarJogada(jogadorO, 0, 1);
        jogo.executarJogada(jogadorX, 1, 1);
        jogo.executarJogada(jogadorO, 2, 0);
        jogo.executarJogada(jogadorX, 2, 1);
        jogo.executarJogada(jogadorO, 1, 0);
        ResultadoJogada resultadoJogada = jogo.executarJogada(jogadorX, 1, 2);

        Assert.assertEquals(ResultadoJogada.ENCERRADO_EMPATADO, resultadoJogada);
        Assert.assertEquals(null, jogo.getVencedor());
        Assert.assertTrue(jogo.jogoEncerrado());
    }
}

